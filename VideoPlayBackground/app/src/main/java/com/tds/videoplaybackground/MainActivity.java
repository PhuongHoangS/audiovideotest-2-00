package com.tds.videoplaybackground;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import com.tds.videoplaybackground.audio.AudioActivity;
import com.tds.videoplaybackground.video.VideoActivity;

public class MainActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Button audio = findViewById(R.id.audio_button);
    Button video = findViewById(R.id.video_button);

    audio.setOnClickListener(v -> {
        Intent intent = new Intent(this, AudioActivity.class);
        startActivity(intent);
    });

    video.setOnClickListener(v -> {
        Intent intent = new Intent(this, VideoActivity.class);
        startActivity(intent);
    });

  }

}
