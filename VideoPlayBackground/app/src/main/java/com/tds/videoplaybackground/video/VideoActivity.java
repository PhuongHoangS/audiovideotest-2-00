/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tds.videoplaybackground.video;

import static com.tds.videoplaybackground.video.Samples.AD_TAG_URI;
import static com.tds.videoplaybackground.video.Samples.MP4_URI;

import android.app.Activity;
import android.os.Bundle;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ads.AdsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.tds.videoplaybackground.R;

public class VideoActivity extends Activity {

  private PlayerView playerView;
  private SimpleExoPlayer player;
  private ImaAdsLoader adsLoader;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_video);

    playerView = findViewById(R.id.player_view);
    adsLoader = new ImaAdsLoader(this, AD_TAG_URI);
      player = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), new DefaultTrackSelector());
      playerView.setPlayer(player);
      adsLoader.setPlayer(player);

      DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(
              this,
              Util.getUserAgent(this, getString(R.string.app_name)));
      ExtractorMediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
              .createMediaSource(MP4_URI);
      MediaSource adsMediaSource =
              new AdsMediaSource(mediaSource, dataSourceFactory, adsLoader, playerView);
      player.prepare(adsMediaSource);

      player.setPlayWhenReady(true);
  }

  @Override
  protected void onStart() {
    super.onStart();
  }

  @Override
  protected void onStop() {
    super.onStop();
  }

  @Override
  protected void onDestroy() {
    adsLoader.release();
    super.onDestroy();
  }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.main_activity);
//
//        playerView = findViewById(R.id.player_view);
//        adsLoader = new ImaAdsLoader(this, AD_TAG_URI);
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//
//        player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector());
//        playerView.setPlayer(player);
//        adsLoader.setPlayer(player);
//
//        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(
//                this,
//                Util.getUserAgent(this, getString(R.string.app_name)));
//        ExtractorMediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
//                .createMediaSource(MP4_URI);
//        MediaSource adsMediaSource =
//                new AdsMediaSource(mediaSource, dataSourceFactory, adsLoader, playerView);
//        player.prepare(adsMediaSource);
//
//        player.setPlayWhenReady(true);
//    }
//
//    @Override
//    protected void onStop() {
//        adsLoader.setPlayer(null);
//        playerView.setPlayer(null);
//        player.release();
//        player = null;
//
//        super.onStop();
//    }

}
