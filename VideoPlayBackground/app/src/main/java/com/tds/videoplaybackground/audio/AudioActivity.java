/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tds.videoplaybackground.audio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.exoplayer2.offline.ProgressiveDownloadAction;
import com.google.android.exoplayer2.util.Util;
import com.tds.videoplaybackground.R;

import static com.tds.videoplaybackground.audio.Samples.SAMPLES;

public class AudioActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_audio);

    Intent intent = new Intent(this, AudioPlayerService.class);
    Util.startForegroundService(this, intent);

    ListView listView = findViewById(R.id.list_view);
    listView.setAdapter(
        new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, SAMPLES));
    listView.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ProgressiveDownloadAction action = new ProgressiveDownloadAction(
            SAMPLES[position].uri, false, null, null);
        AudioDownloadService.startWithAction(
            AudioActivity.this,
            AudioDownloadService.class,
            action,
            false);
      }
    });
  }

}
