/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tds.videoplaybackground.video;

import android.net.Uri;

public final class Samples {

  public static final Uri MP4_URI = Uri.parse("https://storage.googleapis.com/exoplayer-test-media-0/firebase-animation.mp4");
//  public static final Uri MP4_URI = Uri.parse("https://vs.youmaker.com/assets/81d500f9-f4e3-49c3-9a95-ce7e5873fc67/playlist.m3u8");
//  public static final Uri AD_TAG_URI = Uri.parse("https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpostlongpod&cmsid=496&vid=short_tencue&correlator=");
  public static final Uri AD_TAG_URI = Uri.parse("");
}
